// Spike diameter
spike_diameter = 13;

// 
diameter_bottom_multipler = 0.9;
diameter_bottom = spike_diameter*diameter_bottom_multipler;

diameter_mid_multipler = 0.9;
diameter_mid = spike_diameter*diameter_mid_multipler;

// 
diameter_top_multipler = 0.2;
diameter_top = spike_diameter*diameter_top_multipler;

height_1 = spike_diameter*0.1;
height_2 = spike_diameter*0.3;

// number of rows
rows = 9; 
// number of columns
cols = 3; 

diag = sin(30)*spike_diameter/2;
hor = cos(30)*spike_diameter/2;

module hex() {
    union() {
        cylinder(d1 = diameter_bottom, d2 = spike_diameter, h = height_1, $fa = 60);
        translate([0, 0, height_1])
        cylinder(d1 = spike_diameter, d2 = spike_diameter, h = height_1, $fa = 60);
        translate([0, 0, height_1*2])
        cylinder(d1 = spike_diameter, d2 = diameter_mid, h = height_1, $fa = 60);
        translate([0, 0, height_1*3])
        cylinder(d1 = diameter_mid, d2 = diameter_top, h = height_2, $fa = 60);
    }
}


for (i = [0:rows-1])
for (j = [0:cols-1+i%2]) {
    x = j*(spike_diameter+2*diag) + ((i+1)%2)*(spike_diameter/2+diag);
    y = i*hor;
    translate([x, y, 0])
    hex();
}


key_hole_width = 25;
length = 58;
thickness = 3;
height = 5;

difference() {
    hull() {
        translate([-length/2-10, 0, 0])
        cylinder(d=16, h=height, center=true);
        cube([length+2*thickness, key_hole_width+2*thickness, height], center=true);
        translate([length/2+10, 0, 0])
        cylinder(d=16, h=height, center=true);
    }
    hull() {
        translate([0, 0, 2.5])
        cube([length, key_hole_width, 5], center=true);
        cylinder(d=10, h=7, center=true);
    }
    translate([-length/2-10, 0, 0.6])
    cylinder(d=12.4, h=3, center=true);
    translate([length/2+10, 0, 0.6])
    cylinder(d=12.4, h=3, center=true);
    
}
$fs=0.2;

gap_top = 7;
gap_bottom = 3;

length_front = 25;
length_back = 20;

thickness = 3;

hook_angle = 10;
hook_length = 15;

width = 7;

//t1=7;
//t=3;
//w=7;

// 35
// 45
//h = 25;

module components(i) {
    if (i == 1) {
        translate([length_front, 0, 0])
        translate([-sin(hook_angle)*hook_length, cos(hook_angle)*hook_length, 0])
        sphere(d=width);
    }
    if (i == 2) {
        translate([length_front, 0, 0])
        rotate([90, 0, 0])
        cylinder(d=width, h=thickness, center=true);
    }
    if (i == 3) {
        cylinder(d=thickness, h=width, center=true);
    }
    if (i == 4) {
        translate([0, -gap_top-thickness, 0])
        cylinder(d=thickness, h=width, center=true);
    }
    if (i == 5) {
        translate([20, -gap_bottom-thickness, 0])
        cylinder(d=thickness, h=width, center=true);
    }
}

for (i = [1 : 4]) {
    hull() {
        components(i);
        components(i+1);
    }
}

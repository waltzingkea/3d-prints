// small square 55g
// sircle 84g
// square large 160g
// rec long 40g
// rec long 71g
// ali circle 49g

/* [Box] */
// Thickness
t = 3;
height1 = 0.85;
height2 = 0.93;
top = false;
bottom = true;


/* [Cards ]*/

/* 
// playig cards
width = 57.5;
depth = 16.9;
height = 89.4;
*/

/*
// Guess Who
width = 56.2;
depth = 17.8;
height = 87;
*/

/*
// Phase 10
width=56.4; // I have width .3 larger than what is measured. 
depth = 30.7;
height=87.6;
*/

// Uno 
/*
width = 56;
//depth = 31.5; // uno flip
//depth = 17; // junior uno
depth = 31; // uno
height = 87.4;
*/

// Uno
width = 56.5;
//depth = 31.5; // uno flip
//depth = 17; // junior uno
depth = 31; // uno
height = 87.4;

show_cards = false;

//test_ring = true;

/* [Magnet] */
magnet = true;
m_width = 1.05;
m_depth = 5.4;
m_height = 5.4;

/* [Quality] */
$fa=1;
$fs=0.2;

//m_depth = m_depth + 0.2;

c = height*height1;
c2 = height*height2;

intersection() {
    //cube([100,100,10], center=true);
union() {
//color("red", 0.5)
if(bottom) {
difference() {
    union() {
        difference() {
            minkowski() {
                cube([width, depth, height], center=true);
                sphere(r=t);
            }
            cube([width, depth, height], center=true);
            
            translate([0, 0, c])
            cube([width*2, depth*2, height], center=true);
        }
        difference() {
            minkowski() {
                cube([width, depth, height], center=true);
                sphere(r=t/2-0.11);
            }
            cube([width, depth, height], center=true);
            
            translate([0, 0, c2])
            cube([width*2, depth*2, height], center=true);
            
            
            hull() {
                translate([0, 0, c])
                cube([width*2, m_depth+2.2, height], center=true);
                
                translate([0, 0, height])
                cube([width*2, depth+0.2, height], center=true);
            }
            
        }
    } 
    
    
    rotate([90, 0, 0])
    scale([1, 30/17, 1])
    cylinder(h=100,d=17*2);
    
    
    if (magnet) {
        translate([width/2+t/2, 0, -m_height/2-height/2+c+0.01])
        cube([m_width, m_depth+0.2, m_height+0.2], center=true);
        
        translate([-1*(width/2+t/2), 0, -m_height/2-height/2+c+0.01])
        cube([m_width, m_depth+0.2, m_height+0.2], center=true);
    }
 
 
    
    r = 0.2;
    translate([width/2, depth/2, 0])
    cylinder(r=r, h=height, center=true);
    translate([-width/2, depth/2, 0])
    cylinder(r=r, h=height, center=true);
    translate([width/2, -depth/2, 0])
    cylinder(r=r, h=height, center=true);
    translate([-width/2, -depth/2, 0])
    cylinder(r=r, h=height, center=true);
}
}


translate([0, 0, 0.001])
color("yellow", 0.5)
if (top) {
    difference() {
        union() {
            intersection() {
                minkowski() {
                    cube([width, depth, height], center=true);
                    sphere(r=t);
                }
                translate([0, 0, height])
                cube([width*2, depth*2, height], center=true);
            }
            intersection() {
                difference() {
                    minkowski() {
                        cube([width, depth, height], center=true);
                        sphere(r=t);
                    }
                    
                    minkowski() {
                        cube([width, depth, height], center=true);
                        sphere(r=t/2+0.1);
                    }
                }    
                translate([0, 0, c])
                cube([width*2, depth*2, height], center=true);
            }
            
            intersection() {
                difference() {
                    minkowski() {
                        cube([width, depth, height], center=true);
                        sphere(r=t);
                    }
                    
                    minkowski() {
                        cube([width, depth, height], center=true);
                        //sphere(r=t);
                    }
                }    
                hull() {
                    translate([0, 0, c])
                    cube([width*2, m_depth+1.8, height], center=true);
                    
                    translate([0, 0, height])
                    cube([width*2, depth-0.2, height], center=true);
                }
            }
        } 

        if (magnet) {
            translate([width/2+t/2, 0, m_height/2-height/2+c-0.01])
            cube([m_width, m_depth, m_height], center=true);
            
            translate([-1*(width/2+t/2), 0, m_height/2-height/2+c-0.01])
            cube([m_width, m_depth, m_height], center=true);
        }
    }
    
}
}
}

if (show_cards) {
    color("silver", 0.8)
    minkowski() {
        cube([width-5, depth-1, height], center=true);
        rotate([90, 0, 0])
        cylinder(d=5, h=1, center=true);
    }
}


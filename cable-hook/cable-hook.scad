// Cable Hook

// Inner diameter of the hook
id = 30;

thickness = 3; // [2:0.2:5]

width = 10; // [1:20]

// Hook angle
angle = 300; // [180:360]

screw_hole_diameter = 3.4; // [2:0.2:6]

// Translate hole 1 for easier mounting
hole_1_offset = 0; // [-5: 0.2: 5]
// Translate hole 2 for easier mounting
hole_2_offset = 0; // [-5: 0.2: 5]

$fs=0.3;
$fa=2;

arm_l = id/2+thickness+screw_hole_diameter+2+hole_2_offset;

difference() {
    union() {
        rotate_extrude(angle=angle) {
            translate([id/2, 0, 0])
            square([thickness, width]);
        }
        
        translate([cos(angle)*((id+thickness)/2), sin(angle)*((id+thickness)/2), 0])
        cylinder(d=thickness, h=width);
        
        translate([id/2, -arm_l])
        cube([thickness, arm_l, width]);
        
        translate([id/2, -width, 0])
        cube([thickness, width, width+hole_1_offset]);
        
        translate([id/2, -width/2, width+hole_1_offset])
        rotate([0, 90, 0])
        cylinder(h=thickness, d=width);
        
        translate([id/2, -arm_l, width/2])
        rotate([0, 90, 0])
        cylinder(h=thickness, d=width);
    }

    translate([0, -width/2, width+hole_1_offset])
    rotate([0, 90, 0])
    cylinder(h=id, d=screw_hole_diameter);

    translate([id/2-1, -arm_l, width/2])
    rotate([0, 90, 0])
    cylinder(h=thickness+2, d=screw_hole_diameter);
}
output = "leg_adapter"; // [leg_adapter, test_ring, table]

/* [Led adapter] */
leg_diameter = 28.7;  
leg_height = 70;
leg_thickness = 4;
fin_height=0.7; // [0:0.1:1]
screw_hole_diameter = 5.2;
screw_head_diameter = 10;
screw_head_angle = 40; // [0:1:45]

/* [Table] */
table_width = 370;
table_length = 350;
table_height = 18;

/* [STL Quality] */
$fa=1;
$fs=0.2;

module hole() {
    cylinder(d=screw_hole_diameter, h = 10);
    translate([0, 0, -0.01])

    cylinder(d2 = screw_hole_diameter, d1 = screw_head_diameter, h = tan(screw_head_angle)*(screw_head_diameter-screw_hole_diameter)/2);
    translate([0, 0, -10])
    cylinder(d = screw_head_diameter, h = 10);
}

module fin() {
    h = leg_height*fin_height;
    module s() {
        difference() {
            hull() {
                translate([0, 0, -leg_thickness])
                cube([leg_height, leg_diameter/2+leg_thickness, leg_thickness]);
                
                translate([0, 0, -h])
                cube([0.001, leg_diameter/2+leg_thickness, h]);
            }
        }
    }
    
    difference() {
        s();
        
        translate([leg_height/2, leg_diameter/4, -leg_thickness]) 
        hole();
        
        translate([leg_height-10, leg_diameter/4, -leg_thickness])
        hole();
        
        translate([0, -leg_thickness, -leg_thickness])
        s();
    }
}

module leg_adapter() {
    difference() {
        union() {
            translate([0, 0, -leg_height])
            cylinder(d=leg_diameter+2*leg_thickness, h=leg_height);
            
            fin();
            mirror([1, 1, 0])
            fin();
        }
        
        translate([0, 0, -leg_height-1])
        cylinder(d=leg_diameter, h=leg_height+2);
        
        translate([(leg_diameter/2+leg_thickness-1)/2^0.5, -(leg_diameter/2+leg_thickness-1)/2^0.5, 0]) {
            translate([0, 0, -2/3*leg_height])
            rotate([90, 0, 180+45])
            hole();
            
            translate([0, 0, -1/3*leg_height])
            rotate([90, 0, 180+45])
            hole();
        }
       
    }
}

if (output == "leg_adapter") {
    leg_adapter();
}

if (output == "test_ring") {
    difference() {
        cylinder(d=leg_diameter+2*leg_thickness, h=10);
        translate([0, 0, -1])
        cylinder(d=leg_diameter, h=20);
    }
}

if (output == "table") {
    color("SaddleBrown")
    translate([-table_width/2, -table_length/2, 0])
    cube([table_width, table_length, table_height]);
    
    color("Grey"){
        b = leg_diameter/2+leg_thickness;
        translate([table_width/2-b, table_length/2-b])
        rotate([0, 0, -90])
        leg_adapter();

        translate([(table_width/2-b), -(table_length/2-b)])
        rotate([0, 0, 180])
        leg_adapter();
        
        translate([-(table_width/2-b), (table_length/2-b)])
        rotate([0, 0, 0])
        leg_adapter();
        
        translate([-(table_width/2-b), -(table_length/2-b)])
        rotate([0, 0, 90])
        leg_adapter();
    }
}



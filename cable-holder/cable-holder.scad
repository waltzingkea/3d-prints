// Remix of https://www.printables.com/model/695257-cable-holder/files

// Number of cable hooks
count = 4;

// Height of 7 is origional size
height = 7;

screw_mount = true;

screw_hole_diameter = 3.2;

screw_head_diameter = 6;

// Show screws for illustration purpose
show_screws = false; 

$fa=1;
$fs=0.2;

/* Hidden */
scale = height/7;

for ( i = [0 : count-1]) {
    translate([i*14*scale, 0, 0])
    clip();
}
    
if (screw_mount) {
    screw_offset = screw_head_diameter/2+1;
    translate([0, 0, 7.5*scale])
    rotate([-90, 0, 0]) {
        difference() {
            cylinder_scale = (screw_head_diameter+2)/(7.5*scale);
            scale(scale)
            hull() {
                scale([cylinder_scale, 1, 1])
                cylinder(d=15, h=3);
                translate([count*14, 0, 0])
                scale([cylinder_scale, 1, 1])
                cylinder(d=15, h=3);
            }
            
            translate([-screw_offset, 0, -1])
            cylinder(d=screw_hole_diameter+0.2, h=5*scale);
            
            translate([count*14*scale+screw_offset, 0, -1])
            cylinder(d=screw_hole_diameter+0.2, h=5*scale);
        }
        
        color("silver", 0.5)
        if (show_screws) {
            translate([-screw_offset, 0, 0])
            cylinder(d=screw_hole_diameter, h=5);
            translate([-screw_offset, 0, 5])
            cylinder(d=screw_head_diameter, h=2);
            
            translate([count*14*scale+screw_offset, 0, 0])
            cylinder(d=screw_hole_diameter, h=5);
            translate([count*14*scale+screw_offset, 0, 5])
            cylinder(d=screw_head_diameter, h=2);
        }
    }
}    
    
module clip() {
    scale(scale)
    translate([7, 7, 0])
    rotate([0, 0, 180])
    import("CableHolder_v1-1.stl");
}



thickness = 3;
width = 10;

//l1 = 10;

// Height
l2 = 20;

// Length
l3 = 30;

// Clip length
l4 = 5;

clip = false;

screw_hole_diameter = 3;

l1 = width/2;

radius = 1;

/* [STL Quality] */
$fa=2;
$fs=0.3;



module arc(angle, height, inner_radius, outer_radius) {
    rotate_extrude(angle=angle)
    translate([inner_radius, 0, 0])
    square([outer_radius-inner_radius, height]);
}


difference() {
    union() {
        translate([0, 0, width/2])
        rotate([0, 90, 0])
        cylinder(d=width, h=thickness);

        cube([thickness, l1, width]);
    }
    
    translate([-1, 0, width/2])
    rotate([0, 90, 0])
    cylinder(d=screw_hole_diameter, h=thickness+2);
}

translate([radius+thickness, l1, 0])
rotate([0, 0, 90])
arc(angle = 90, height = width, inner_radius = radius, outer_radius =radius+thickness);

translate([radius+thickness, l1+radius, 0])
cube([l2-2*radius-thickness, thickness, width]);

translate([l2-radius, l1+2*radius+thickness, 0])
rotate([0, 0, -90])
arc(angle = 90, height = width, inner_radius = radius, outer_radius =radius+thickness);

translate([l2, l1+radius+thickness+radius])
cube([thickness, l3-2*radius, width]);

translate([l2-radius, l1+thickness+l3, 0])
rotate([0, 0, 0])
arc(angle = 90, height = width, inner_radius = radius, outer_radius =radius+thickness);

translate([l2-radius-l4, l1+l3+radius+thickness, 0])
cube([l4, thickness, width]);

if (clip) {
    translate([l2-radius-l4, l1+l3+radius+thickness+thickness/2, 0])
    cylinder(d=thickness, h=width);
} else {
    translate([radius+thickness, l1+radius+thickness+l3, 0])
    cube([l2-2*radius-thickness, thickness, width]);
    
    translate([radius+thickness, l1+2*radius+thickness+thickness+l3, 0])
    rotate([0, 0,180])
    arc(angle = 90, height = width, inner_radius = radius, outer_radius =radius+thickness);
    
    difference() {
        union() {
            translate([0, l1+l3+2*radius+2*thickness])
            cube([thickness, l1, width]);
            
            translate([0, l1+radius+thickness+l3+thickness+radius+l1, width/2])
            rotate([0, 90, 0])
            cylinder(d=width, h=thickness);
        }
        
        translate([-1, l1+radius+thickness+l3+thickness+radius+l1, width/2])
        rotate([0, 90, 0])
        cylinder(d=screw_hole_diameter, h=2+thickness);
    }
}